# Vim Cheat Sheet

## Copy
`yy` Copy the line
`Ctrl+v` Visual mode, select text, `y` copies it

## Cut
`dd` Cut line
`d$` Cut to the end of line
`d0` Cut to the beginning of line
`d2d` Cut 2 lines

## Paste
`p` paste after cursor
`P` paste before curser

## Move
`fb` Move to next `b`
`Fb` Move to previous `b`
`tb` Move to before the next `b`
`Tb` Move to before previous `b`
`gg` Move to beginning of the file
`GG` Move to end of file
`22gg` Move to line `22`
`$` Move to end of line

## Vimvoodoo
`:wq !sudo tee %` Save as sudo 
