# Docker Cheat Sheet

`sudo docker info` View details about current Docker installation/state
`sudo docker run <container-name>` Run a container
`sudo docker image ls` Lists local Docker images
`sudo container ls --all` Lists Docker containers history

## Docker cleanup
`docker system prune --volumes` Clean everything.
`docker container prune` Clean stopped containers.
`docker image prune -a` Clean all unused images.
`docker volume prune` Clean all unused volumes.
`docker network prune` Clean all unused networks.
When images won't disapear:
``` shell
sudo systemctl stop docker
sudo rm -rf /var/lib/docker
sudo systemctl start docker
```
