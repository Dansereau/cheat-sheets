# Cheat sheets for terminal

## Description

This repository has multiple cheat sheets to facilitate the use of terminal commands and program.
They are intended to be used with aliases directly in the terminal.
Example: `alias csTerminator='pygmentize -g -O style=paraiso-dark /path/to/repo/terminator.md`
