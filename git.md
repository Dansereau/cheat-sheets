# Git Cheat Sheet

## General tools to help you with git

- `man giteveryday`
A useful minimum set of commands for Everyday Git
- `man gittutorial`  
A tutorial introduction to Git
- `man gittutorial-2`  
A tutorial introduction to Git: part two
- `man gitworkflows`  
An overview of recommended workflows with Git
- Show current branch when working in the terminal: [Instructions here](https://www.leaseweb.com/labs/2013/08/git-tip-show-your-branch-name-on-the-linux-prompt/).

## Working with branches

* List branches in your local directory: `git branch`
* List branches in your local and remote directory: `git branch -a`
* Create a new branch on your local machine and switch in this branch:  
`git checkout -b <branchName>`
* Push a new local branch to the remote directory: `git push origin <branchName>`
* Switch to an existing branch in your local directory: `git checkout <branchName>`
* Switch to an existing remote branch that is not in your local repository:  
`git fetch`  
`git branch <branchName> origin/<remoteBranchName>`  
*The __branchName__ is the name the branch will have in your local repository and the _remoteBranchName_ is the actual name of the branch on GitLab.*
* Delete a local branch: `git branch -d <branchName>`
* Delete a remote branch: `git push origin --delete <branchName>`

## Commit

* Commit section of a file:  
*If your change are to large or not related to each other, you
can commit only part of the file.*  
`git add -p <fileName>` 
* Commit a single file: `git commit -m"<yourMessageHere>" <path>/<yourFileName>`
* Cancel a commit that hasn't been pushed yet:
`git reset HEAD~`  
*[Find more information here.](https://stackoverflow.com/questions/927358/how-to-undo-the-most-recent-commits-in-git)*
* Modify a commit message: `git commit --amend`  
*The existing message will open in your default text editor. Modify and save.*

## Rebase
Rebase is a really powerfull tool, but it can't be use in every situation. You should never rebase if you're not sure that no one else is working on the same branch as you.

### Correcting rebase mistake
* To see the official commit history:
```git hist```
* To see the unofficial commit history (including rebase mistake):
```git reflog```  

## History

Search for string in history `git log -S<string>` to show the diffs add `-p` to search all branches add `--all`
