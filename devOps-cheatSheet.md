# DevOps Cheat Sheet

## User management

Change default shell

``` bash
chsh -s `which` <your_new_shell>
```

`usermod -aG <group> <user>` Add a user to a secondary group

`getent passwd | cut -d: -f1` List users
