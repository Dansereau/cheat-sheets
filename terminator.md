# Terminator Cheat Sheet

## Split Open Close
`Ctrl+Shift+O`	Split terminals Horizontally. 
`Ctrl+Shift+E`	Split terminals Vertically. 
`Ctrl+Shift+W`	Close the current terminal. 
`Ctrl+Shift+Q`	Quits Terminator 
`Ctrl+Shift+T`	Open new tab 
`Ctrl+Shift+I`	Open a new window.
`Super+i`    	Spawn a new Terminator process

## Move and resize (within a tab)
`Ctrl+Shift+Right` or `+Left` or `+Up` or `+Down` Move parent dragbar. 
`Ctrl+Shift+Tab`	Move to previous terminal.
`Alt+Up` or `Alt+Down` or `Alt+Left` or `Alt+Right`
	Move to the terminal up, down, left or right of the current one. 
`Ctrl+Shift+X`		Toggle between showing all terminals and only showing the current one (maximise). 

## Zoom
`Ctrl+Shift+Plus (+)`	Increase font size.
`Ctrl+Minus (-)`	Decrease font size.
`Ctrl+Zero (0)`		Restore font size to original setting. 

## Tabs
`Ctrl+PageDown`		Move to next Tab 
`Ctrl+PageUp`		Move to previous Tab 
`Ctrl+Shift+PageDown`	Swap tab position with next Tab 
`Ctrl+Shift+PageUp`	Swap tab position with previous Tab 

## Group
`Super+g`    Group all terminals so input goes to all of them. 
	`Super+Shift+G` to cancel.
`Super+t`    Group all terminals in current tab so input goes to terminals in current tab. 
	`Super+Shift+T` to cancel.

## Other
`Ctrl+Shift+S`	Hide/Show Scrollbar. 
`Ctrl+Shift+F`	Search within terminal scrollback 
`F11`		Toggle fullscreen 
