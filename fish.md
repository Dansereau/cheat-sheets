# Fish Shell Cheat Sheet

`(**pwd**)` Command interpolation where `pwd` is the command

## Nice Fish Shortcut

`ctrl+u` Backward-kill-line.
`ctrl+w` Backward-kill-path-component.
`ctrl+a` Beginning of line.
`ctrl+e` End of line.

## Variable

`set variableName value`

## Aliases

`cl` Clear terminal.
`l` List files & folders.
`fzfp` Fuzzy finder with file preview.
`cats` Pretty cat.
`nAnsRole $destiantion` Create a new ansible role to the specified destination.
`fire` Launches Firefox.
`fishConfig` Edit config.fish.

### Git Aliases

`gits` Git status.
`gitco` Git checkout.
`gitb` Git branch.
`gitc $message` Git commit -am.
`gitp` Git push.
`gitaa` Git add .

### Cheat Sheet Aliases

`csDevOps`
`csDevOpsEdit`
`csTerminator`
`csVim`
`csDocker`
`csDockerEdit`
`csFirefox`
`csFish`
`csFishEdit`
`csGit`
`csGitEdit`

`newCheatSheet $name` Creates a new cheat sheet (Requires to add alias manually).

### Docker Aliases
`dockls` List running containers.
`docklsa` List all containers, including inactives.
`dockerclean` Remove all dangling containers
`dockip` Show ip of container passed as args (id or name).
`dockipAll` List all ips of currently running containers.